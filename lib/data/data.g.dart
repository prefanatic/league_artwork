// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'data.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ContentData _$ContentDataFromJson(Map<String, dynamic> json) {
  return ContentData(
      assetGroupTypes: (json['assetGroupTypes'] as List)
          ?.map((e) => e == null
              ? null
              : AssetGroupType.fromJson(e as Map<String, dynamic>))
          ?.toList(),
      assetTypes: (json['assetTypes'] as List)
          ?.map((e) =>
              e == null ? null : AssetType.fromJson(e as Map<String, dynamic>))
          ?.toList(),
      assetGroups: (json['assetGroups'] as List)
          ?.map((e) =>
              e == null ? null : AssetGroup.fromJson(e as Map<String, dynamic>))
          ?.toList(),
      assets: (json['assets'] as List)
          ?.map((e) =>
              e == null ? null : Asset.fromJson(e as Map<String, dynamic>))
          ?.toList(),
      defaultProfile: json['defaultProfile'] == null
          ? null
          : DefaultProfile.fromJson(
              json['defaultProfile'] as Map<String, dynamic>),
      collections: json['collections'] == null
          ? null
          : Collections.fromJson(json['collections'] as Map<String, dynamic>),
      versionDate: json['versionDate'] as String,
      locale: json['locale'] == null
          ? null
          : Locale.fromJson(json['locale'] as Map<String, dynamic>));
}

Map<String, dynamic> _$ContentDataToJson(ContentData instance) =>
    <String, dynamic>{
      'assetGroupTypes': instance.assetGroupTypes,
      'assetTypes': instance.assetTypes,
      'assetGroups': instance.assetGroups,
      'assets': instance.assets,
      'defaultProfile': instance.defaultProfile,
      'collections': instance.collections,
      'versionDate': instance.versionDate,
      'locale': instance.locale
    };

Locale _$LocaleFromJson(Map<String, dynamic> json) {
  return Locale(
      locales: (json['locales'] as List)?.map((e) => e as String)?.toList(),
      translations: (json['translations'] as Map<String, dynamic>)?.map(
        (k, e) => MapEntry(
            k,
            (e as Map<String, dynamic>)?.map(
              (k, e) => MapEntry(k, e as String),
            )),
      ));
}

Map<String, dynamic> _$LocaleToJson(Locale instance) => <String, dynamic>{
      'locales': instance.locales,
      'translations': instance.translations
    };

Collections _$CollectionsFromJson(Map<String, dynamic> json) {
  return Collections(
      featured: (json['featured'] as List)?.map((e) => e as String)?.toList(),
      collections: (json['collections'] as List)
          ?.map((e) =>
              e == null ? null : Collection.fromJson(e as Map<String, dynamic>))
          ?.toList());
}

Map<String, dynamic> _$CollectionsToJson(Collections instance) =>
    <String, dynamic>{
      'featured': instance.featured,
      'collections': instance.collections
    };

Collection _$CollectionFromJson(Map<String, dynamic> json) {
  return Collection(
      id: json['id'] as String,
      coverAssetId: json['coverAssetId'] as String,
      categories:
          (json['categories'] as List)?.map((e) => e as String)?.toList(),
      tags: (json['tags'] as List)?.map((e) => e as String)?.toList(),
      nameTranslateId: json['nameTranslateId'] as String);
}

Map<String, dynamic> _$CollectionToJson(Collection instance) =>
    <String, dynamic>{
      'id': instance.id,
      'coverAssetId': instance.coverAssetId,
      'categories': instance.categories,
      'tags': instance.tags,
      'nameTranslateId': instance.nameTranslateId
    };

DefaultProfile _$DefaultProfileFromJson(Map<String, dynamic> json) {
  return DefaultProfile(
      animationDuration: json['animationDuration'] as int,
      imageDuration: json['imageDuration'] as int,
      displayMirroring: json['displayMirroring'] as bool,
      assets: (json['assets'] as List)?.map((e) => e as String)?.toList(),
      id: json['id'] as int);
}

Map<String, dynamic> _$DefaultProfileToJson(DefaultProfile instance) =>
    <String, dynamic>{
      'animationDuration': instance.animationDuration,
      'imageDuration': instance.imageDuration,
      'displayMirroring': instance.displayMirroring,
      'assets': instance.assets,
      'id': instance.id
    };

Asset _$AssetFromJson(Map<String, dynamic> json) {
  return Asset(
      id: json['id'] as String,
      dateAdded: json['dateAdded'] as String,
      url: json['url'] as String,
      tags: (json['tags'] as List)?.map((e) => e as String)?.toList(),
      type: json['type'] as String,
      size: json['size'] as int,
      color: json['color'] as String,
      thumbnailUrl: json['thumbnailUrl'] as String,
      thumbnailSize: json['thumbnailSize'] as int,
      nameTranslateId: json['nameTranslateId'] as String);
}

Map<String, dynamic> _$AssetToJson(Asset instance) => <String, dynamic>{
      'id': instance.id,
      'dateAdded': instance.dateAdded,
      'url': instance.url,
      'tags': instance.tags,
      'type': instance.type,
      'size': instance.size,
      'color': instance.color,
      'thumbnailUrl': instance.thumbnailUrl,
      'thumbnailSize': instance.thumbnailSize,
      'nameTranslateId': instance.nameTranslateId
    };

AssetGroup _$AssetGroupFromJson(Map<String, dynamic> json) {
  return AssetGroup(
      id: json['id'] as String,
      previewUrl: json['previewUrl'] as String,
      dateAdded: json['dateAdded'] as String,
      assets: (json['assets'] as List)?.map((e) => e as String)?.toList(),
      tags: (json['tags'] as List)?.map((e) => e as String)?.toList(),
      nameTranslateId: json['nameTranslateId'] as String,
      previewThumbnailUrl: json['previewThumbnailUrl'] as String,
      previewThumbnailSize: json['previewThumbnailSize'] as int);
}

Map<String, dynamic> _$AssetGroupToJson(AssetGroup instance) =>
    <String, dynamic>{
      'id': instance.id,
      'previewUrl': instance.previewUrl,
      'dateAdded': instance.dateAdded,
      'assets': instance.assets,
      'tags': instance.tags,
      'nameTranslateId': instance.nameTranslateId,
      'previewThumbnailUrl': instance.previewThumbnailUrl,
      'previewThumbnailSize': instance.previewThumbnailSize
    };

AssetType _$AssetTypeFromJson(Map<String, dynamic> json) {
  return AssetType(
      id: json['id'] as String,
      nameTranslateId: json['nameTranslateId'] as String);
}

Map<String, dynamic> _$AssetTypeToJson(AssetType instance) => <String, dynamic>{
      'id': instance.id,
      'nameTranslateId': instance.nameTranslateId
    };

AssetGroupType _$AssetGroupTypeFromJson(Map<String, dynamic> json) {
  return AssetGroupType(
      id: json['id'] as String,
      showAlphabet: json['showAlphabet'] as bool,
      nameTranslateId: json['nameTranslateId'] as String);
}

Map<String, dynamic> _$AssetGroupTypeToJson(AssetGroupType instance) =>
    <String, dynamic>{
      'id': instance.id,
      'showAlphabet': instance.showAlphabet,
      'nameTranslateId': instance.nameTranslateId
    };
