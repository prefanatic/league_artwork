import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:json_annotation/json_annotation.dart';

part 'package:league_artwork/data/data.g.dart';

const _contentDataUrl =
    'http://screensavers.riotgames.com/latest/content/data.json';

Future<ContentData> fetchContentData() async {
  final http.Response response = await http.get(Uri.parse(_contentDataUrl));
  final ContentData data = ContentData.fromJson(json.decode(response.body));

  return data;
}

@JsonSerializable()
class ContentData {
  final List<AssetGroupType> assetGroupTypes;
  final List<AssetType> assetTypes;
  final List<AssetGroup> assetGroups;
  final List<Asset> assets;
  final DefaultProfile defaultProfile;
  final Collections collections;
  final String versionDate;
  final Locale locale;

  ContentData({
    this.assetGroupTypes,
    this.assetTypes,
    this.assetGroups,
    this.assets,
    this.defaultProfile,
    this.collections,
    this.versionDate,
    this.locale,
  });

  factory ContentData.fromJson(Map<String, dynamic> json) =>
      _$ContentDataFromJson(json);
}

@JsonSerializable()
class Locale {
  final List<String> locales;
  final Map<String, Map<String, String>> translations;

  Locale({
    this.locales,
    this.translations,
  });

  factory Locale.fromJson(Map<String, dynamic> json) => _$LocaleFromJson(json);
}

@JsonSerializable()
class Collections {
  final List<String> featured;
  final List<Collection> collections;

  Collections({this.featured, this.collections});

  factory Collections.fromJson(Map<String, dynamic> json) =>
      _$CollectionsFromJson(json);
}

@JsonSerializable()
class Collection {
  final String id;
  final String coverAssetId;
  final List<String> categories;
  final List<String> tags;
  final String nameTranslateId;

  Collection({
    this.id,
    this.coverAssetId,
    this.categories,
    this.tags,
    this.nameTranslateId,
  });

  factory Collection.fromJson(Map<String, dynamic> json) =>
      _$CollectionFromJson(json);
}

@JsonSerializable()
class DefaultProfile {
  final int animationDuration;
  final int imageDuration;
  final bool displayMirroring;
  final List<String> assets;
  final int id;

  DefaultProfile({
    this.animationDuration,
    this.imageDuration,
    this.displayMirroring,
    this.assets,
    this.id,
  });

  factory DefaultProfile.fromJson(Map<String, dynamic> json) =>
      _$DefaultProfileFromJson(json);
}

@JsonSerializable()
class Asset {
  final String id;
  final String dateAdded;
  final String url;
  final List<String> tags;
  final String type;
  final int size;
  final String color;
  final String thumbnailUrl;
  final int thumbnailSize;
  final String nameTranslateId;

  Uri get uri => Uri.http(
        'screensavers.riotgames.com',
        'latest/content/$url',
      );

  Uri get thumbnailUri => Uri.http(
        'screensavers.riotgames.com',
        'latest/content/$thumbnailUrl',
      );

  Asset({
    this.id,
    this.dateAdded,
    this.url,
    this.tags,
    this.type,
    this.size,
    this.color,
    this.thumbnailUrl,
    this.thumbnailSize,
    this.nameTranslateId,
  });

  factory Asset.fromJson(Map<String, dynamic> json) => _$AssetFromJson(json);

  @override
  String toString() {
    return 'Asset{id: $id, dateAdded: $dateAdded, url: $url, tags: $tags,'
        ' type: $type, size: $size, color: $color, thumbnailUrl: $thumbnailUrl,'
        ' thumbnailSize: $thumbnailSize, nameTranslateId: $nameTranslateId}';
  }
}

@JsonSerializable()
class AssetGroup {
  final String id;
  final String previewUrl;
  final String dateAdded;
  final List<String> assets;
  final List<String> tags;
  final String nameTranslateId;
  final String previewThumbnailUrl;
  final int previewThumbnailSize;

  Uri get previewUri => Uri.http(
        'screensavers.riotgames.com',
        'latest/content/thumbnail/asset-group/$previewUrl',
      );

  AssetGroup({
    this.id,
    this.previewUrl,
    this.dateAdded,
    this.assets,
    this.tags,
    this.nameTranslateId,
    this.previewThumbnailUrl,
    this.previewThumbnailSize,
  });

  factory AssetGroup.fromJson(Map<String, dynamic> json) =>
      _$AssetGroupFromJson(json);

  @override
  String toString() {
    return 'AssetGroup{id: $id, previewUrl: $previewUrl, dateAdded: $dateAdded,'
        ' assets: $assets, tags: $tags, nameTranslateId: $nameTranslateId,'
        ' previewThumbnailUrl: $previewThumbnailUrl,'
        ' previewThumbnailSize: $previewThumbnailSize}';
  }
}

@JsonSerializable()
class AssetType {
  final String id;
  final String nameTranslateId;

  AssetType({this.id, this.nameTranslateId});

  factory AssetType.fromJson(Map<String, dynamic> json) =>
      _$AssetTypeFromJson(json);
}

@JsonSerializable()
class AssetGroupType {
  final String id;
  final bool showAlphabet;
  final String nameTranslateId;

  AssetGroupType({this.id, this.showAlphabet, this.nameTranslateId});

  factory AssetGroupType.fromJson(Map<String, dynamic> json) =>
      _$AssetGroupTypeFromJson(json);
}
