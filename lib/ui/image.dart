import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:league_artwork/data/data.dart';
import 'package:league_artwork/main.dart';

class HeroImage extends StatelessWidget {
  final Uri previewUri;
  final Uri uri;
  final bool thumbnail;

  final Size size;

  const HeroImage({
    Key key,
    this.previewUri,
    this.uri,
    this.thumbnail = true,
    @required this.size,
  }) : super(key: key);

  HeroImage.asset(
    Asset asset, {
    bool thumbnail,
  }) : this(
          previewUri: asset.thumbnailUri,
          uri: asset.uri,
          thumbnail: thumbnail,
          size: Size(424, 227),
        );

  @override
  Widget build(BuildContext context) {
    return Hero(
      tag: previewUri,
      flightShuttleBuilder: (
        BuildContext flightContext,
        Animation<double> animation,
        HeroFlightDirection flightDirection,
        BuildContext fromHeroContext,
        BuildContext toHeroContext,
      ) {
        // Overriding flightShuttleBuilder to always display the full
        // high-resolution image, so that pop directions scale down
        // the image back to thumbnail size.
        return CachedNetworkImage(
          fadeInDuration: const Duration(),
          imageUrl: uri.toString(),
          cacheManager: cacheManager,
        );
      },
      // Wrapping the image in an AspectRatio prevents a bug with Hero,
      // where an async Image load results in a width and height of 0, 0
      // for the Hero during it's RectTween calculation.  AspectRatio will
      // trick Hero and use the appropriate ending Rect, as it's predefined.
      child: AspectRatio(
        aspectRatio: size.aspectRatio,
        child: CachedNetworkImage(
          fit: BoxFit.fill,
          width: size.width,
          height: size.height,
          fadeInDuration: const Duration(),
          imageUrl: (thumbnail ? previewUri : uri).toString(),
          cacheManager: cacheManager,
          placeholder: (context, url) {
            return Center(child: CircularProgressIndicator());
          },
        ),
      ),
    );
  }
}
