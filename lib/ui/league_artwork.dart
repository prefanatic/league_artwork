import 'package:flutter/material.dart';
import 'package:league_artwork/ui/about.dart';
import 'package:league_artwork/domain/curator.dart';
import 'package:league_artwork/data/data.dart';
import 'package:league_artwork/ui/hero_route.dart';
import 'package:league_artwork/ui/detail.dart';
import 'package:league_artwork/ui/home.dart';
import 'package:provider/provider.dart';

class LeagueArtwork extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        FutureProvider(
          initialData: null,
          create: (context) => fetchContentData(),
        ),
        ChangeNotifierProvider(create: (context) => Curator())
      ],
      child: MaterialApp(
        title: 'Flutter Demo',
        theme: ThemeData(
          primarySwatch: Colors.teal,
          scaffoldBackgroundColor: Colors.white30,
          brightness: Brightness.dark,
        ),
        routes: {
          '/': (context) => Home(),
          '/about': (context) => AboutScreen(),
          '/assets': (context) => AssetList(),
        },
        onGenerateRoute: (settings) {
          if (settings.name == '/detail') {
            return HeroDialogRoute(builder: (_) {
              return AssetDetail(
                asset: settings.arguments,
              );
            });
          }

          return null;
        },
      ),
    );
  }
}
