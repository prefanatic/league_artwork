import 'dart:ui' as ui;

import 'package:flutter/material.dart';
import 'package:league_artwork/domain/curator.dart';
import 'package:league_artwork/data/data.dart';
import 'package:league_artwork/ui/image.dart';
import 'package:provider/provider.dart';

const categoryChampionPreviewSize = Size(210, 300);
const assetChampionPreviewSize = Size(424, 0);

Size _sizeForGroupType(AssetGroupType type) {
  switch (type.id) {
    case 'champions':
      return categoryChampionPreviewSize;
    default:
      return categoryChampionPreviewSize;
  }
}

String getTranslation(
  String key,
  ui.Locale locale,
  Map<String, Map<String, String>> translations,
) {
  final mapForLocale = translations[locale.toString()];
  if (mapForLocale == null) return key;
  return mapForLocale[key] ?? key;
}

class Home extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final curator = Provider.of<Curator>(context);
    final data = Provider.of<ContentData>(context);
    if (data == null) {
      return Scaffold(body: Center(child: CircularProgressIndicator()));
    }

    final navigationItems = data.assetGroupTypes.map((type) {
      return BottomNavigationBarItem(
        title: Text(type.id),
        icon: Icon(Icons.person),
      );
    }).toList();

    return Scaffold(
      body: GroupList(),
      bottomNavigationBar: BottomNavigationBar(
        currentIndex: curator.groupIndex,
        items: navigationItems,
        onTap: curator.setGroupIndex,
      ),
    );
  }
}

class GroupList extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final curator = Provider.of<Curator>(context);
    final data = Provider.of<ContentData>(context);
    if (data == null) {
      return Scaffold(body: Center(child: CircularProgressIndicator()));
    }

    final groupType = data.assetGroupTypes[curator.groupIndex];
    final List<AssetGroup> filtered = data.assetGroups.where((group) {
      return group.tags.contains(groupType.id);
    }).toList();

    final assetSize = _sizeForGroupType(groupType);

    return GridView.builder(
      gridDelegate: SliverGridDelegateWithMaxCrossAxisExtent(
        maxCrossAxisExtent: assetSize.width,
        childAspectRatio: assetSize.aspectRatio,
        mainAxisSpacing: 8.0,
        crossAxisSpacing: 8.0,
      ),
      itemCount: filtered.length,
      itemBuilder: (context, index) {
        final asset = filtered[index];

        return Stack(
          alignment: Alignment.center,
          fit: StackFit.passthrough,
          children: [
            _GridImage(
              title: getTranslation(asset.nameTranslateId,
                  Localizations.localeOf(context), data.locale.translations),
              previewUri: asset.previewUri,
              size: Size(210, 300),
            ),
            Material(
              type: MaterialType.transparency,
              child: InkWell(
                onTap: () {
                  curator.setRequestedAssets(asset.assets);
                  Navigator.pushNamed(context, '/assets');
                },
              ),
            )
          ],
        );
      },
    );
  }
}

class AssetList extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final curator = Provider.of<Curator>(context);
    final data = Provider.of<ContentData>(context);
    if (data == null) {
      return Scaffold(body: Center(child: CircularProgressIndicator()));
    }

    final List<Asset> filtered = data.assets
        .where((asset) => curator.requestedAssets.contains(asset.id))
        .where((asset) => asset.id.contains(curator.searchFilter))
        .toList();

    return Scaffold(
      appBar: AppBar(
        title: Text('Assets'),
        actions: <Widget>[
          SizedBox(
            width: 200.0,
            child: Center(
              child: TextField(onChanged: curator.setSearch),
            ),
          ),
          IconButton(
            icon: Icon(Icons.help),
            onPressed: () {
              Navigator.pushNamed(context, '/about');
            },
          ),
        ],
      ),
      body: GridView.builder(
        gridDelegate: SliverGridDelegateWithMaxCrossAxisExtent(
          maxCrossAxisExtent: 424.0,
          childAspectRatio: 1.8678,
          mainAxisSpacing: 8.0,
          crossAxisSpacing: 8.0,
        ),
        itemCount: filtered.length,
        itemBuilder: (context, index) {
          final curator = Provider.of<Curator>(context);
          final asset = filtered[index];

          return Stack(
            alignment: Alignment.center,
            fit: StackFit.passthrough,
            children: [
              _GridImage(
                previewUri: asset.thumbnailUri,
                uri: asset.uri,
                color: Color(
                  int.tryParse(asset.color?.substring(1, 7) ?? 'FF000000',
                          radix: 16) +
                      0xFF000000,
                ),
                size: Size(424, 227),
              ),
              Material(
                type: MaterialType.transparency,
                child: InkWell(
                  onTap: () async {
                    print(asset);
                    await curator.ensureAssetStored(asset.uri);
                    if (curator.isAssetInTransit(asset.uri)) return;

                    Navigator.pushNamed(
                      context,
                      '/detail',
                      arguments: asset,
                    );
                  },
                ),
              ),
              if (curator.isAssetInTransit(asset.uri))
                Center(child: CircularProgressIndicator()),
            ],
          );
        },
      ),
    );
  }
}

class _GridImage extends StatelessWidget {
  final String title;
  final Color color;
  final Uri previewUri;
  final Uri uri;
  final Size size;

  const _GridImage({
    Key key,
    this.title = '',
    this.color = Colors.black26,
    this.previewUri,
    this.uri,
    @required this.size,
  }) : super(key: key);

  _GridImage.asset(Asset asset)
      : this(
          title: asset.id,
          previewUri: asset.thumbnailUri,
          uri: asset.uri,
          color: Color(
            int.tryParse(asset.color?.substring(1, 7) ?? 'FF000000',
                    radix: 16) +
                0xFF000000,
          ),
          size: Size(424, 227),
        );

  @override
  Widget build(BuildContext context) {
    return ClipRRect(
      borderRadius: BorderRadius.circular(0.0),
      child: GridTile(
        footer: GridTileBar(
          title: Text(title),
          backgroundColor: color.withOpacity(0.8),
        ),
        child: HeroImage(
          previewUri: previewUri,
          uri: uri,
          size: size,
        ),
      ),
    );
  }
}
