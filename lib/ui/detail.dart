import 'dart:ui' as ui;

import 'package:flutter/material.dart';
import 'package:league_artwork/data/data.dart';
import 'package:league_artwork/ui/image.dart';

class AssetDetail extends StatelessWidget {
  final Asset asset;

  const AssetDetail({
    Key key,
    this.asset,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ClipRect(
      child: Scaffold(
        backgroundColor: Colors.transparent,
        appBar: AppBar(),
        body: Center(
          child: BackdropFilter(
            filter: ui.ImageFilter.blur(sigmaX: 4, sigmaY: 4),
            child: HeroImage.asset(asset, thumbnail: false),
          ),
        ),
      ),
    );
  }
}
