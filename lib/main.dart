import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:league_artwork/ui/league_artwork.dart';
import 'package:flutter_cache_manager/flutter_cache_manager.dart';

final cacheManager = DefaultCacheManager();

void main() {
  debugDefaultTargetPlatformOverride = TargetPlatform.android;

  runApp(LeagueArtwork());
}
