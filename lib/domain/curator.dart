import 'package:flutter/material.dart';
import 'package:league_artwork/main.dart';

class Curator with ChangeNotifier {
  List<Uri> _assetsInTransit = [];

  String _searchFilter = '';

  String get searchFilter => _searchFilter;

  void setSearch(String search) {
    _searchFilter = search;
    notifyListeners();
  }

  int _groupIndex = 0;

  int get groupIndex => _groupIndex;

  void setGroupIndex(int index) {
    _groupIndex = index;
    notifyListeners();
  }

  List<String> _requestedAssets = [];

  List<String> get requestedAssets => _requestedAssets;

  void setRequestedAssets(List<String> assets) {
    _requestedAssets = assets;
    notifyListeners();
  }

  bool isAssetInTransit(Uri uri) => _assetsInTransit.contains(uri);

  Future<bool> isAssetStored(Uri uri) async {
    return await cacheManager.getFileFromCache(uri.toString()) != null;
  }

  Future<bool> ensureAssetStored(Uri uri) async {
    if (isAssetInTransit(uri)) return true;
    if (await isAssetStored(uri)) return true;

    _assetsInTransit.add(uri);
    notifyListeners();
    await cacheManager.getSingleFile(uri.toString());
    _assetsInTransit.remove(uri);
    notifyListeners();
    return true;
  }
}
