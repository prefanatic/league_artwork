import 'package:flutter_test/flutter_test.dart';
import 'package:league_artwork/data/data.dart';

void main() {
  test('Should deserialize', () async {
    final data = await fetchContentData();

    expect(data.versionDate, equals('Wed Nov 22 2017 05:03:36 GMT+0000 (UTC)'));

    data.assets.map((asset) => asset.id).forEach(print);
  });
}
